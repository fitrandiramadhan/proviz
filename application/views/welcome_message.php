<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>amCharts examples</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
	<script
	src="https://code.jquery.com/jquery-2.2.4.js"
	integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
	crossorigin="anonymous"></script>

	<!-- Home URL : localhost/proviz -->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/amcharts.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/pie.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/amcharts/3.13.0/themes/dark.js"></script>

	<script>

		var chart;
		var legend;
		var headers = [
			'Continent', 'Country', 'Games per User', 
			'Hours (per 2 weeks)', 'Most Ownd', 'Favorite Games'
		];
		var defaultDatas = [];

		$(document).ready(function() {
			$.ajax({
				type: "GET",
				url: "data.txt",
				dataType: "text",
				success: function(data) {
					processData(data);
					createLegendByData();
				}
			});
		});

		function processData(allText, selector) {
			defaultDatas = $.csv.toObjects(allText);

			var parsedDatas = [];
			parsedDatas = parseTotal(defaultDatas, 'Continent');
			makeChart(parsedDatas);
		}

		$(document).on('change', 'input[name="input-by-legend"]', function () {
			if ($(this).val() === 'Most Ownd' || $(this).val() === 'Favorite Games') {
				var temp = [];
				for (var g = 1; g < 6; g++) {
					var stringSelector = $(this).val() + (String)(g);
					temp = parseTotal(defaultDatas, stringSelector, temp);
				}
				parsedDatas = temp;
			} else {
				parsedDatas = parseTotal(defaultDatas, $(this).val());
			}
			chart.dataProvider = parsedDatas;
			chart.validateData();
			chart.validateNow();
		});

		function createLegendByData() {
			for (var i = 0; i < headers.length; i++) {
				var inputRadioByData = $('<input>').attr('type', 'radio').attr('name', 'input-by-legend').attr('value', headers[i]);
				$('.legend-by-data').append(inputRadioByData).append(headers[i]);
			}
		}

		function parseTotal(datas, selector, ref) {
			var newDatas = [];
			
			if (ref) {
				newDatas = ref;
			}

			for (var i = 0; i < datas.length; i++) {

				var eachSelector = {
					"selectorTitle": datas[i][selector],
					"selectorValue": 1
				};

				// Special case
				if (selector === 'Hours (per 2 weeks)') {
					// Hours per 2 weeks
					if (parseInt(datas[i][selector]) > 40) {
						eachSelector.selectorTitle = 'Above 40 hours';
					} else if (parseInt(datas[i][selector]) > 30 && parseInt(datas[i][selector]) <= 40) {
						eachSelector.selectorTitle = 'Between 30 - 40 hours';
					} else if (parseInt(datas[i][selector]) > 20 && parseInt(datas[i][selector]) <= 30) {
						eachSelector.selectorTitle = 'Between 20 - 30 hours';
					} else if (parseInt(datas[i][selector]) > 10 && parseInt(datas[i][selector]) <= 20) {
						eachSelector.selectorTitle = 'Between 10 - 20 hours';
					} else {
						eachSelector.selectorTitle = 'Below 10 hours';
					}
				}

				var found = false;
				var group = [];
				if (newDatas) {
					for (var key in newDatas) {

						if (selector === 'Hours (per 2 weeks)') {
							if (parseInt(datas[i][selector]) > 40 && newDatas[key].selectorTitle === 'Above 40 hours') {
								found = true;
								newDatas[key].selectorValue += 1;
							} else if (parseInt(datas[i][selector]) > 30 && parseInt(datas[i][selector]) <= 40 && newDatas[key].selectorTitle === 'Between 30 - 40 hours') {
								found = true;
								newDatas[key].selectorValue += 1;
							} else if (parseInt(datas[i][selector]) > 20 && parseInt(datas[i][selector]) <= 30 && newDatas[key].selectorTitle === 'Between 20 - 30 hours') {
								found = true;
								newDatas[key].selectorValue += 1;
							} else if (parseInt(datas[i][selector]) > 10 && parseInt(datas[i][selector]) <= 20 && newDatas[key].selectorTitle === 'Between 10 - 20 hours') {
								found = true;
								newDatas[key].selectorValue += 1;
							} else if (parseInt(datas[i][selector]) <= 10 && newDatas[key].selectorTitle === 'Below 10 hours') {
								found = true;
								newDatas[key].selectorValue += 1;
							}
						} else if (newDatas[key].selectorTitle === datas[i][selector]) {
							found = true;
							newDatas[key].selectorValue += 1;
						}

						if (found) {
							break;
						}
					}
				}

				if (!found) {
					newDatas.push(eachSelector);
				}
			}

			return newDatas;
		}

		function makeChart(datas, title, value) {
			// PIE CHART
			chart = new AmCharts.AmPieChart();
			chart.theme = 'dark';
			chart.dataProvider = datas;
			chart.titleField = "selectorTitle";
			chart.valueField = "selectorValue";
			chart.gradientRatio = [0, 0, 0 ,-0.2, -0.4];
			chart.gradientType = "radial";
			// chart.color = [
			// 	"#FF0F00", "#FF6600", "#FF9E01", 
			// 	"#FCD202", "#F8FF01", "#B0DE09", 
			// 	"#04D215", "#0D8ECF", "#0D52D1", 
			// 	"#2A0CD0", "#8A0CCF", "#CD0D74", 
			// 	"#754DEB", "#DDDDDD", "#999999", 
			// 	"#333333", "#000000", "#57032A", 
			// 	"#CA9726", "#990000", "#4B0C25"
			// ];

			// LEGEND
			legend = new AmCharts.AmLegend();
			legend.position = 'right';
			legend.marginRight = 100;
			// legend.autoMargins = true;
			legend.align = "center";
			legend.markerType = "circle";
			chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
			chart.addLegend(legend);

			// WRITE
			chart.write("chartdiv");
		}

		// changes label position (labelRadius)
		function setLabelPosition() {
			if (document.getElementById("rb1").checked) {
				chart.labelRadius = 30;
				chart.labelText = "[[title]]: [[value]]";
			} else {
				chart.labelRadius = -30;
				chart.labelText = "[[percents]]%";
			}
			chart.validateNow();
		}


		// makes chart 2D/3D
		function set3D() {
			if (document.getElementById("rb3").checked) {
				chart.depth3D = 10;
				chart.angle = 10;
			} else {
				chart.depth3D = 0;
				chart.angle = 0;
			}
			chart.validateNow();
		}

		// changes switch of the legend (x or v)
		function setSwitch() {
			if (document.getElementById("rb5").checked) {
				legend.switchType = "x";
			} else {
				legend.switchType = "v";
			}
			legend.validateNow();
		}
	</script>
</head>
<body>
	<div id="container" style="margin-top: 100px;">
		<div id="chartdiv" style="width: 100%; height: 768px;"></div>
		<table align="center" cellspacing="20">
			<tr>
				<td>
					<input type="radio" checked="true" name="group" id="rb1" onclick="setLabelPosition()">labels outside
					<input type="radio" name="group" id="rb2" onclick="setLabelPosition()">labels inside
				</td>
				<td>
					<input type="radio" name="group2" id="rb3" onclick="set3D()">3D
					<input type="radio" checked="true" name="group2" id="rb4" onclick="set3D()">2D
				</td>
				<td>Legend switch type:
					<input type="radio" checked="true" name="group3" id="rb5" onclick="setSwitch()">x
					<input type="radio" name="group3" id="rb6" onclick="setSwitch()">v</td>
			</tr>
		</table>
		<p style="text-align: center;">Legend by data</p>
		<div class="legend-by-data" style="text-align: center;">
			
		</div>
			
	</div>
</body>
</html>
