from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
import argparse
import urllib
from bs4 import BeautifulSoup
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--finput", required = True,
	help = "Path to the file that contains appid")
ap.add_argument("-i", "--index", required = True,
	help = "Start number index appid")
ap.add_argument("-o", "--output", required = True,
	help = "Output filename XML")
args = vars(ap.parse_args())

skip_data = False
def set_skip_data(state):
    global skip_data
    skip_data = state

def read_list_appid(input_data):
    data = []
    listappid = open(input_data,'r')
    for line in listappid:
        line = line.replace('\n','')
        data.append(str(line))
    
    listappid.close()

    return data

def read_url(appid):
    response = urllib.urlopen("http://store.steampowered.com/app/"+appid)
    return response.read()

def get_title(data):
    title = data.find_all("span", attrs={"itemprop": "name"})

    if title:
        title = title[0].text
    else:
        set_skip_data(True)

    return title

def get_release_date(data):
    release_date = data.find_all("span", class_='date')

    if release_date:
        release_date = release_date[0].text
    else:
        set_skip_data(True)

    return release_date

def get_genre(data):
    genre = data.find_all("div", class_="details_block")
    genre = str(genre)
    s = BeautifulSoup(genre, "html.parser")
    genre = s.select('a[href^="http://store.steampowered.com/genre/"]')
    return genre

def get_os(data):
    data_os_win = data.find_all("div", attrs={"data-os":"win"})
    data_os_mac = data.find_all("div", attrs={"data-os":"mac"})
    data_steam_os = data.find_all("div", attrs={"data-os":"linux"})

    console = ''

    if data_os_win:
        console = console + 'win'

    if data_os_mac:
        console = console + ',mac'

    if data_steam_os:
        console = console + ',steamplay'
    
    return console

def get_tags(data):
    tags = data.find_all("a", class_="app_tag")
    
    tags_ = []
    for tag_ in tags:
        tag_ = ''.join(e for e in tag_.text if e.isalnum())
        tags_.append(tag_)

    return tags_

def get_reviews(data):
    reviews = ['0','0']

    positive_reviews = data.find_all("label", attrs={"for":"review_type_positive"})
    negative_reviews = data.find_all("label", attrs={"for":"review_type_negative"})

    if positive_reviews:
        positive_reviews = str(positive_reviews)
        pr = BeautifulSoup(positive_reviews, "html.parser")
        positive_reviews = pr.find_all("span")

        reviews[0] = positive_reviews[0].text
        reviews[0] = reviews[0].replace('(','')
        reviews[0] = reviews[0].replace(')','')
    
    if negative_reviews:
        negative_reviews = str(negative_reviews)
        nr = BeautifulSoup(negative_reviews, "html.parser")
        negative_reviews = nr.find_all("span")

        reviews[1] = negative_reviews[0].text
        reviews[1] = reviews[1].replace('(','')
        reviews[1] = reviews[1].replace(')','')
    
    return reviews

def convert_to_xml(datagame, appid, number, title, release, genres, os, tags, reviews):
    app = SubElement( datagame, 'appid', attrib={"id":appid, "no":number} )

    SubElement( app, 'title' ).text = str(title)
    SubElement( app, 'release' ).text = str(release)
    
    genres_ = SubElement( app, 'genres' )
    i = 1
    for genre in genres:
        SubElement( genres_, 'genre', attrib={"no":str(i)} ).text = str(genre.text)
        i = i + 1
    
    SubElement(app, 'os').text = str(os)

    tags_ = SubElement( app, 'tags' )
    i = 1
    for tag in tags:
        SubElement( tags_, 'tag', attrib={"no":str(i)} ).text = str(tag)
        i = i + 1

    reviews_ = SubElement( app, 'reviews' )
    SubElement( reviews_, 'positive' ).text = str(reviews[0])
    SubElement( reviews_, 'negative' ).text = str(reviews[1])

    return app


#### MAIN ####
input_filename = args["finput"]
output_filename = args["output"]

# <datagame/>
datagame = Element( 'datagame' )

i = int(args["index"])
success = 0
failed = 0
for appid in read_list_appid(input_filename):
    data = BeautifulSoup(read_url(appid), "html.parser")

    title = get_title(data)
    release = get_release_date(data)
    genres = get_genre(data)
    os = get_os(data)
    tags = get_tags(data)
    reviews = get_reviews(data)

    if skip_data:
        failed = failed + 1
        skip_data = False
        print '\n', failed, ' failed! skip data: ', appid, '\n'
        continue
    else:
        i = i + 1
        success = success + 1
        print i, ' ---- get data: ', appid
        
        add_dataxml = convert_to_xml(datagame, appid, str(i), title, release, genres, os, tags, reviews)
        print 'add to xml success!'


# write xml file
output_file = open( output_filename, 'w' )
output_file.write( '<?xml version="1.0"?>' )
output_file.write( ElementTree.tostring( datagame, encoding="utf-8", method="html" ) )
output_file.close()

print output_filename, 'Done. Failed = ', failed, ' Success = ', (success-failed)